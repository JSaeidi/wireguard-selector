from os import system as run
from glob import glob
from time import sleep
from tqdm import tqdm
run("rm *.zip_test")
for number,this in enumerate(glob('*.conf')):
    print(this,'>>', number)
    up_command = 'sudo wg-quick up ' + ''.join(this.split('.conf')[:-1])
    down_command = 'sudo wg-quick down ' + ''.join(this.split('.conf')[:-1])
    wget_command = 'wget -q -O ' + this + '.zip_test' + ' https://speed.hetzner.de/100MB.bin --no-check-certificate &'
    axel_command = f'axel -k -q -n 4 https://speed.hetzner.de/100MB.bin -o {this}.zip_test &'
    print([down_command])
    run(up_command)
    run(axel_command)
    
    for counter in tqdm(range(25)):
        sleep(1)
    run('killall wget')
    run('killall axel')
    run(down_command)
    sleep(1)
